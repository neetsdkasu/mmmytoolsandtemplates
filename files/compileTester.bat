@if not "%~1"=="" goto compile
@for %%f in (*Vis.java) do call "%~0" "%%f"
@goto endlabel

:compile
@if not exist tmpclasses ( mkdir tmpclasses )
javac -d tmpclasses "%~1"

@if ERRORLEVEL 1 ( goto endlabel )

@if exist tester.jar ( goto updatejar ) else ( goto newjar )

:updatejar

jar -uvf tester.jar -C tmpclasses .

@goto endlabel

:newjar

jar -cvfe tester.jar %~n1 -C tmpclasses .

@goto endlabel


:endlabel
