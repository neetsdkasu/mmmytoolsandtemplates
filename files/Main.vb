Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports Console = System.Console
Imports Stopwatch = System.Diagnostics.Stopwatch

Public Module Main

    Dim sw As New Stopwatch()

    Sub PrintTime()
        Console.Error.WriteLine("time: {0} ms", sw.ElapsedMilliseconds)
    End Sub

    Sub Main(args() As String)
        Try
            sw.Start()

            Dim sl As New Solution()

            sw.Stop()

            CallSolve(sl)

        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

    Sub CallSolve(sl As Solution)
        Dim N As Integer = CInt(Console.ReadLine())
        Dim data(N - 1) As Integer
        For i As Integer = 0 To UBound(data)
            data(i) = CInt(Console.ReadLine())
        Next i

        sw.Start()

        Dim ret() As Integer = sl.solve(data)

        sw.Stop()
        PrintTime()

        Console.WriteLine(ret.Length)
        For Each r As Integer In ret
            Console.WriteLine(r)
        Next r
        Console.Out.Flush()
    End Sub

End Module