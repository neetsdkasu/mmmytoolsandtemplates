#!/bin/sh

function foo() {
	x=$(($1%100))
	y=$(($x%10))
	if test $y -eq 1 -a $x -ne 11; then
		echo $1st
	elif test $y -eq 2 -a $x -ne 12; then
		echo $1nd
	elif test $y -eq 3 -a $x -ne 13; then
		echo $1rd
	else
		echo $1th
	fi
}

echo "0=$0, 1=$1, 2=$2"

a=1
b=$1
if test "$2" != ""; then
	a=$1
	b=$2
fi

if test "$1" = ""; then
	echo fuga
else	
	for ((i=$a;i<=$b;i++)); do
		echo '-----------------------------'
		git checkout local-`foo $i`
		./compile.bat
		./doPublish.bat $i local 10
	done

fi
