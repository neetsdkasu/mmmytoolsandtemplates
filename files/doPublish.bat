@echo off

if "%~1"=="/?" goto ERRLABEL1
if "%~1"=="-?" goto ERRLABEL1
if "%~1"=="/h" goto ERRLABEL1
if "%~1"=="-h" goto ERRLABEL1
if "%~1"=="/help" goto ERRLABEL1
if "%~1"=="--help" goto ERRLABEL1

if "%~1"=="local" goto LOCNEW
if "%~2"=="local" goto LOCUPD

if not exist public mkdir public

if "%~1"=="" goto PUBNEWDEF
if "%~1"=="--" goto PUBNEWH
if "%~2"=="" goto PUBUPDDEF
if "%~2"=="--" goto PUBUPDH

goto PUBUPD

:ERRLABEL1

echo usage:
echo     %~nx0 [local] [-- endSeed]
echo     %~nx0 [local] -- startSeed endSeed [args]
echo         make result with new submit number
echo     %~nx0 subnum [local] [[--] endSeed]
echo     %~nx0 subnum [local] [--] startSeed endSeed [args]
echo         make result with special submit number
echo ...each usages use default f.bat seeds ^(1 to 10^)

exit /B 0

:LOCNEW
REM  cmd local ? ? ? ?

if "%~2"=="" goto LOCNEWDEF
if "%~2"=="--" goto LOCNEWH
if not "%~4"=="" goto LOCNEWA
if not "%~3"=="" goto LOCNEWE

echo local new ^(1 to %2^)

setlocal ENABLEDELAYEDEXPANSION
if ERRORLEVEL 1 (
    echo failure! LOCNEWDEF "setlocal"
    endlocal
    exit /B 1
)
set X=0
for /L %%I IN (1,1,100) DO (
    if "!X!"=="0" (
        if not exist "local\submit%%I" (
            set X=%%I
        )
    )
)
if "!X!"=="0" (
    echo failure! LOCNEWDEF
    exit /B 1
)
echo mkdir local\submit!X!
mkdir local\submit!X!
echo f.bat %2 ^> local\submit!X!\score.txt
call f.bat %2 > local\submit!X!\score.txt
echo done!
endlocal
exit /B 0


:LOCNEWA
:LOCNEWE
:LOCNEWH
REM  cmd local -- ? ? ?

echo failure! LOCNEWH
exit /B 1


:PUBNEWDEF
REM  cmd

for /L %%I IN (1,1,20) DO (
    if not exist "public\submit%%I" (
        echo mkdir public\submit%%I
        exit /B 0
    )
)
echo failure! PUBNEWDEF
exit /B 1


:LOCNEWDEF
REM  cmd local

echo local new default

if not exist local mkdir local

setlocal ENABLEDELAYEDEXPANSION
if ERRORLEVEL 1 (
    echo failure! LOCNEWDEF "setlocal"
    endlocal
    exit /B 1
)
set X=0
for /L %%I IN (1,1,100) DO (
    if "!X!"=="0" (
        if not exist "local\submit%%I" (
            set X=%%I
        )
    )
)
if "!X!"=="0" (
    echo failure! LOCNEWDEF
    exit /B 1
)
echo mkdir local\submit!X!
mkdir local\submit!X!
echo f.bat ^> local\submit!X!\score.txt
call f.bat > local\submit!X!\score.txt
echo done!
endlocal
exit /B 0


:PUBNEWH
REM  cmd -- ? ? ? 

echo failure! PUBNEWH
exit /B 1


:PUBUPDDEF
REM  cmd subnum

echo failure! PUBUPDDEF
exit /B 1

:LOCUPD
REM  cmd subnum local ? ? ? ?

if not exist local mkdir local

if "%~3"=="" goto LOCUPDDEF
if "%~3"=="--" goto LOCUPDH
if not "%~5"=="" goto LOCUPDA
if not "%~4"=="" goto LOCUPDE

echo local update %1 ^(1 to %3^)

if not exist "local\submit%~1" (
    echo not found local\submit%~1
    exit /B 1
)

echo f.bat %3 ^> local\submit%~1\score.txt
f.bat %3 > local\submit%~1\score.txt
echo done!
exit /B 0

:LOCUPDE

echo local update %1 ^(%3 to %4^)

if not exist "local\submit%~1" (
    echo not found local\submit%~1
    exit /B 1
)

echo f.bat %3 %4 ^> local\submit%~1\score.txt
f.bat %3 %4 > local\submit%~1\score.txt
echo done!
exit /B 0

:LOCUPDA

echo local update %1 ^(%3 to %4^) with args

if not exist "local\submit%~1" (
    echo not found local\submit%~1
    exit /B 1
)

setlocal
set args=%5 %6 %7 %8 %9
echo f.bat %3 %4 args ^> local\submit%~1\score.txt
f.bat %3 %4 %args% > local\submit%~1\score.txt
endlocal
echo done!
exit /B 0


:LOCUPDH

echo failure! LOCUPD
exit /B 1


:LOCUPDDEF
REM  cmd subnum local

echo local update %1 default

if not exist "local\submit%~1" (
    echo not found local\submit%~1
    exit /B 1
)

echo f.bat ^> local\submit%~1\score.txt
f.bat > local\submit%~1\score.txt
echo done!
exit /B 0

:PUBUPDH
REM  cmd subnum -- ? ? ?

echo failure! PUBUPDH
exit /B 1

:PUBUPD
REM  cmd subnum ? ? ?

echo failure! PUBUPD
exit /B 1

:ENDLABEL
echo DODO DONE!
exit /B 0