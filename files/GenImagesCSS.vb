Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports System
Imports System.IO
Imports System.Drawing
Imports System.Text

Public Module GenImagesCSS

    Sub ShowUsage()
        Console.Error.WriteLine("usage: GenImagesCSS.exe [[<minSeed>] <maxSeed>] [-d <dir>] [-h]")
        Console.Error.WriteLine()
        Console.Error.WriteLine(" <minSeed> ... minSeed (default 1)")
        Console.Error.WriteLine(" <maxSeed> ... maxSeed (default 10)")
        Console.Error.WriteLine(" -d <dir>  ... image files dir")
        Console.Error.WriteLine(" -h        ... show usage")
    End Sub

    Public Sub Main()
        Try
            Dim args() As String = Environment.GetCommandLineArgs()

            Dim outPath As String = "images.css"
            Dim baseDir As String = ""
            Dim minSeed As Integer = 1
            Dim maxSeed As Integer = -1
            Dim argi As Integer = 1
            Do While argi < args.Length
                Dim tk As String = args(argi)
                Select Case tk
                Case "-help", "-h", "/?", "/h", "/help"
                    ShowUsage()
                    Exit Sub
                Case "-dir", "-d", "/dir", "/d"
                    argi += 1
                    baseDir = args(argi)
                Case Else
                    If maxSeed < 0 Then
                        maxSeed = CInt(tk)
                    Else
                        minSeed = maxSeed
                        maxSeed = CInt(tk)
                    End If
                End Select
                argi += 1
            Loop

            If maxSeed < 0 Then maxSeed = 10

            Dim sb As New StringBuilder()

            For seed As Integer = minSeed To maxSeed
                Dim imgPath As String = Path.Combine(baseDir, CStr(seed) + ".png")
                If Not File.Exists(imgPath) Then
                    Dim tmpPath = Path.Combine(baseDir, CStr(seed) + ".jpg")
                    If Not File.Exists(tmpPath) Then
                        Console.Error.WriteLine("Not found file ({0} or {1})", imgPath, tmpPath)
                        Exit Sub
                    End If
                    imgPath = tmpPath
                End If
                Dim w, h As Integer
                Using img As New Bitmap(imgPath)
                    If img.Width > img.Height Then
                        w = 230
                        h = (img.Height * 230 + img.Width - 1) \ img.Width
                    Else
                        h = 230
                        w = (img.Width * 230 + img.Height - 1) \ img.Height
                    End If
                End Using
                sb.AppendFormat(".example{0} {{ width: {1}px; height: {2}px; }}", seed, w, h)
                sb.AppendLine()
            Next seed

            File.WriteAllText(outPath, sb.ToString())

            Console.WriteLine("saved {0}", outPath)
        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

End Module