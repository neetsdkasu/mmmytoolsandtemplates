@echo off
setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
if ERRORLEVEL 1 (
    echo failure! "setlocal"
    goto FINALLY
)

if "%~1"=="/?" goto SHOWUSAGE
if "%~1"=="-?" goto SHOWUSAGE
if "%~1"=="/h" goto SHOWUSAGE
if "%~1"=="-h" goto SHOWUSAGE
if "%~1"=="/help" goto SHOWUSAGE
if "%~1"=="--help" goto SHOWUSAGE

set baseDir=public
set startSeed=1
set endSeed=10
set submitNum=0
set args=
set msg=NEW

if "%~1"=="local" (
    set baseDir=local
    if "%~2"=="--" (
        goto SETSEED3
    ) else (
        if not "%~2"=="" (
            goto SETSEED2
        )
    )
) else (
    if "%~1"=="--" (
        goto SETSEED2
    ) else (
        if not "%~1"=="" (
            set submitNum=%1
            set msg=UPDATE
            if "%~2"=="local" (
                set baseDir=local
                if "%~3"=="--" (
                    goto SETSEED4
                ) else (
                    if not "%~3"=="" (
                        goto SETSEED3
                    )
                )
            ) else (
                if "%~2"=="--" (
                    goto SETSEED3
                ) else (
                    if not "%~2"=="" (
                        goto SETSEED2
                    )
                )
            )
        )
    )
)

:SEARCHSUBNUM
if "!submitNum!"=="0" (
    for /L %%I IN (1,1,200) DO (
        if "!submitNum!"=="0" (
            if not exist "!baseDir!\submit%%I" (
                set submitNum=%%I
            )
        )
    )
    if "!submitNum!"=="0" (
        echo TOO MANY SUBMIT
        goto FINALLY
    )
)

rem goto CHECKARGS
goto RUNTEST

:FINALLY
endlocal
goto :EOF

:SHOWUSAGE
echo usage:
echo     %~nx0 [-- endSeed]
echo     %~nx0 -- startSeed endSeed [args]
echo     %~nx0 local [[--] endSeed]
echo     %~nx0 local [--] startSeed endSeed [args]
echo         make result with new submit number
echo     %~nx0 subnum [local] [[--] endSeed]
echo     %~nx0 subnum [local] [--] startSeed endSeed [args]
echo         make result with special submit number
echo ...each usages use default f.bat seeds ^(1 to 10^)
goto FINALLY

:CHECKARGS
echo baseDir   "!baseDir!"
echo startSeed "!startSeed!"
echo endSeed   "!endSeed!"
echo submitNum "!submitNum!"
echo args      "!args!"
echo msg       "!msg!"
goto FINALLY

:SETSEED2
if "%~3"=="" (
    set endSeed=%2
) else (
    set startSeed=%2
    set endSeed=%3
    set args=%4 %5 %6 %7 %8 %9
)
goto SEARCHSUBNUM

:SETSEED3
if "%~4"=="" (
    set endSeed=%3 
) else (
    set startSeed=%3
    set endSeed=%4
    set args=%5 %6 %7 %8 %9
)
goto SEARCHSUBNUM

:SETSEED4
if "%~5"=="" (
    set endSeed=%4
) else (
    set startSeed=%4
    set endSeed=%5
    set args=%6 %7 %8 %9
)
goto SEARCHSUBNUM

:RUNTEST
set destDir=!baseDir!\submit!submitNum!
set destFile=!destDir!\score.txt
if "!msg!"=="UPDATE" (
    if not exist !destDir! (
        echo not found "!destDir!"
        goto FINALLY
    )
) else (
    if exist !destDir! (
        echo already existed !destDir!
        goto FINALLY
    )
    mkdir !destDir!
)
echo !msg! Submit !submitNum! ( !startSeed! to !endSeed! ) "!args!"
call f.bat !startSeed! !endSeed! !args! > !destFile!
echo done
goto FINALLY
