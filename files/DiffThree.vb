Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.IO
Imports System.Collections.Generic
Imports Tp = System.Tuple(Of Double, Integer)

Public Module DiffScore

    Private Sub ReadData( _
            reader As TextReader, _
            scores As List(Of Double), _
            ByVal skips As Integer, _
            Optional writer As StreamWriter = Nothing)
        Do
            Dim s As String = reader.ReadLine()
            
            If s Is Nothing Then Exit Do
            
            If writer IsNot Nothing Then
                writer.WriteLine(s)
            End If

            Dim ss() As String = s.Split(" "c)

            If ss.Length <> 3 Then Continue Do
            If ss(1) <> "=" Then Continue Do
            If ss(0) <> "Score" AndAlso ss(0) <> "score" Then Continue Do
            
            If skips > 0 Then
                skips -= 1
                Continue Do
            End If

            Dim sc As Double = CDbl(ss(2))
            scores.Add(sc)
        Loop
    End Sub
    
    Private Function CalcRate(sc1 As Double, sc2 As Double) As Double
        If Math.Sign(sc1) <> 0 Then
            CalcRate = (sc2 - sc1) / sc1
        ElseIf sc1 = sc2 Then
            CalcRate = 0.0
        Else
            CalcRate = Double.PositiveInfinity
        End If
    End Function
    
    Private Sub ShowUsage()
        Console.Error.WriteLine("usage")
        Console.Error.WriteLine("  args: [-skip N] file1 file2 [file3 ...]")
    End Sub

    Public Sub Main()
        Try
            Dim args() As String = Environment.GetCommandLineArgs()

            ' args(0) Is ProgramFile ?
            If args.Length < 3 Then
                ShowUsage()
                Exit Sub
            End If
            
            Dim N As Integer = args.Length - 1
            Dim skips As Integer = 0
            Dim offset As Integer = 1
            
            If args(1)(0) = "-"c Then
                If args(1).ToLower() <> "-skip" Then
                    ShowUsage()
                    Exit Sub
                End If
                If Not Integer.TryParse(args(2), skips) Then
                    ShowUsage()
                    Exit Sub
                End If
                offset = 3
                N -= 2
                If N < 2 Then
                    ShowUsage()
                    Exit Sub
                End If
            End If
            
            
            Dim inputs(N - 1) As String
            Dim scores(N - 1) As List(Of Double)
            Dim totalScores(N - 1) As Double
            Dim caseCount As Integer = Integer.MaxValue

            For i As Integer = 0 To N - 1
                inputs(i) = args(i + offset)
                scores(i) = New List(Of Double)()
                totalScores(i) = 0.0
                Using reader As StreamReader = File.OpenText(inputs(i))
                    ReadData(reader, scores(i), skips)
                End Using
                caseCount = Math.Min(caseCount, scores(i).Count)
            Next i
            
            If caseCount = 0 Then
                Console.Error.WriteLine("no testcases")
                Exit Sub
            End If
            
            For c As Integer = 0 To caseCount - 1
                Dim maxScore As Double = 0.0
                For i As Integer = 0 To N - 1
                    maxScore = Math.Max(maxScore, scores(i)(c))
                Next i
                If maxScore = 0.0 Then
                    Console.Error.WriteLine("case {0} maxScore is 0", c + skips + 1)
                    For i As Integer = 0 To N - 1
                        totalScores(i) += 1.0
                    Next i
                Else
                    For i As Integer = 0 To N - 1
                        totalScores(i) += scores(i)(c) / maxScore
                    Next i
                End If
            Next c
            
            Dim ranking As New List(Of Tp)()
            
            For i As Integer = 0 To N - 1
                totalScores(i) /= CDbl(caseCount)
                ranking.Add(New Tp(totalScores(i), i))
            Next i
            
            ranking.Sort()
            
            Console.WriteLine("rank  score            file")
            For j As Integer = 0 To N - 1
                Dim i As Integer = ranking((N - 1) - j).Item2
                Console.WriteLine("{0,3:D} {1,18:F08} {2}", 
                    j + 1, totalScores(i), inputs(i))
            Next j
            
        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

End Module