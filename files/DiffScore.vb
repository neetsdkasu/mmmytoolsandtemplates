Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.IO
Imports System.Collections.Generic

Public Module DiffScore

    Private Sub ReadData( _
            reader As TextReader, _
            scores As List(Of Double), _
            ByVal skips As Integer, _
            Optional writer As StreamWriter = Nothing)
        Do
            Dim s As String = reader.ReadLine()
            
            If s Is Nothing Then Exit Do
            
            If writer IsNot Nothing Then
                writer.WriteLine(s)
            End If

            Dim ss() As String = s.Split(" "c)

            If ss.Length <> 3 Then Continue Do
            If ss(1) <> "=" Then Continue Do
            If ss(0) <> "Score" AndAlso ss(0) <> "score" Then Continue Do
            
            If skips > 0 Then
                skips -= 1
                Continue Do
            End If

            Dim sc As Double = CDbl(ss(2))
            scores.Add(sc)
        Loop
    End Sub
    
    Private Function CalcRate(sc1 As Double, sc2 As Double) As Double
        If Math.Sign(sc1) <> 0 Then
            CalcRate = (sc2 - sc1) / sc1
        ElseIf sc1 = sc2 Then
            CalcRate = 0.0
        Else
            CalcRate = Double.PositiveInfinity
        End If
    End Function
    
    Private Sub ShowUsage()
        Console.Error.WriteLine("usage")
        Console.Error.WriteLine("  args: [-skip N] file1 file2")
        Console.Error.WriteLine(" or")
        Console.Error.WriteLine("  args: [-skip N] file1   (with Stdin)")
    End Sub

    Public Sub Main()
        Try
            Dim args() As String = Environment.GetCommandLineArgs()

            ' args(0) Is ProgramFile ?
            If args.Length < 2 OrElse 5 < args.Length Then
                ShowUsage()
                Exit Sub
            End If
            
            Dim filemode As Boolean = False
            Dim input1 As String = ""
            Dim input2 As String = ""
            
            Select Case args.Length
            Case 2, 4
                input1 = args(UBound(args))
            Case 3, 5
                input1 = args(UBound(args) - 1)
                input2 = args(UBound(args))
                filemode = True
            Case Else
                ShowUsage()
                Exit Sub
            End Select
            
            Dim skips As Integer = 0
            If args.Length > 3 Then
                If args(1).ToLower() <> "-skip" Then
                    ShowUsage()
                    Exit Sub
                End If
                If Not Integer.TryParse(args(2), skips) Then
                    ShowUsage()
                    Exit Sub
                End If
            End If

            Dim score1 As New List(Of Double)()
            Dim zeros1 As Integer = 0
            Dim minus1 As Integer = 0
            Dim min1 As Double = Double.MaxValue
            Dim max1 As Double = 0.0

            Using reader As StreamReader = File.OpenText(input1)
                ReadData(reader, score1, skips)
            End Using

            Dim score2 As New List(Of Double)()
            Dim zeros2 As Integer = 0
            Dim minus2 As Integer = 0
            Dim min2 As Double = Double.MaxValue
            Dim max2 As Double = 0.0

            If filemode Then
                Using reader As StreamReader = File.OpenText(input2)
                    ReadData(reader, score2, skips)
                End Using
            Else
                Using writer As StreamWriter = File.CreateText("_stdin.txt")
                    ReadData(Console.In, score2, skips,writer)
                End Using
            End If

            Dim n As Integer = Math.Min(score1.Count, score2.Count)
            
            For i As Integer = 0 To n - 1
                Select Case Math.Sign(score1(i))
                Case -1
                    minus1 += 1
                Case 0
                    zeros1 += 1
                Case 1
                    min1 = Math.Min(min1, score1(i))
                    max1 = Math.Max(max1, score1(i))
                End Select
                Select Case Math.Sign(score2(i))
                Case -1
                    minus2 += 1
                Case 0
                    zeros2 += 1
                Case 1
                    min2 = Math.Min(min2, score2(i))
                    max2 = Math.Max(max2, score2(i))
                End Select
            Next i

            If n = 0 Then
                Console.Error.WriteLine("no data")
                Console.Error.WriteLine("file1 data size: {0}", score1.Count)
                Console.Error.WriteLine("file2 data size: {0}", score2.Count)
                Exit Sub
            End If

            Dim file1 As String = Path.GetFileNameWithoutExtension(input1)
            Dim file2 As String = If(filemode, Path.GetFileNameWithoutExtension(input2), "stdin")
            If file1.Length > 18 Then file1 = file1.Substring(0, 18)
            If file2.Length > 18 Then file2 = file2.Substring(0, 18)

            Console.WriteLine("score change summary from {0} to {1}", file1, file2)
            Console.WriteLine("     {0,-18}  {1,-18}  {2,-18}  {3,-8}", _
                file1, file2, "diff", "rate")

            Dim plus As Integer = 0   ' sc1 < sc2 count
            Dim minus As Integer = 0  ' sc1 > sc2 count
            Dim sum1 As Double = 0.0
            Dim sum2 As Double = 0.0
            Dim beat1 As Integer = 0  ' win 10, draw 5
            Dim beat2 As Integer = 0
            Dim rel1 As Double = 0.0  ' sc1 / best
            Dim rel2 As Double = 0.0  ' sc2 / best
            Dim relSQ1 As Double = 0.0  ' (sc1 / best) ^ 2
            Dim relSQ2 As Double = 0.0  ' (sc2 / best) ^ 2
            Dim rateSum As Double = 0.0
            Dim rateCount As Integer = 0
            Dim per As Double = 0.0
            
            For i As Integer = 0 To n - 1
                Dim sc1 As Double = score1(i)
                Dim sc2 As Double = score2(i)
                Select Case Math.Sign(sc2 - sc1)
                Case -1 ' sc1 > sc2
                    minus += 1
                    beat1 += 10
                    If sc1 <> 0.0 Then
                        rel1 += 1.0
                        rel2 += sc2 / sc1
                        relSQ1 += 1.0
                        relSQ2 += sc2 * sc2 / sc1 / sc1
                    Else ' avoid 0 divide
                        rel1 += 1.0
                        relSQ1 += 1.0
                        ' rel2 and relSQ2 += 0.0
                    End If
                    
                Case 1 ' sc1 < sc2
                    plus += 1
                    beat2 += 10
                    If sc2 <> 0.0 Then
                        rel1 += sc1 / sc2
                        rel2 += 1.0
                        relSQ1 += sc1 * sc1 / sc2 / sc2
                        relSQ2 += 1.0
                    Else ' avoid 0 divide
                        rel2 += 1.0
                        relSQ2 += 1.0
                        ' rel1 and relSQ1 += 0.0
                    End If
                    
                Case 0 ' sc1 = sc2
                    beat1 += 5
                    beat2 += 5
                    rel1 += 1.0
                    rel2 += 1.0
                    relSQ1 += 1.0
                    relSQ2 += 1.0
                End Select
                
                per = CalcRate(sc1, sc2)
                If per < Double.PositiveInfinity Then
                    rateSum += per
                    rateCount += 1
                End If
                
                Console.WriteLine( _
                    "{3,4:D}:  {0,18:F08}  {1,18:F08}  {2,18:F08}  {4,8:F04}", _
                    sc1, sc2, sc2 - sc1, i + 1, per)
                
                sum1 += sc1
                sum2 += sc2
                
                If (i + 1) Mod 5 = 0 AndAlso (i + 1) <> n Then
                    Console.WriteLine("-------------------------------------------------------------------")
                End If
                
            Next i
            
            If rateCount > 0 Then
                Console.WriteLine("-------------------------------------------------------------------")
                Console.WriteLine(" rate avg:  {0,63:F04}", rateSum / CDbl(rateCount))
            End If

            Console.WriteLine("-------------------------------------------------------------------")
            Console.WriteLine(" sum:  {0,18:F08}  {1,18:F08}  {2,18:F08}  {3,8:F04}", _
                sum1, sum2, sum2 - sum1, CalcRate(sum1, sum2))

            Dim avg1 As Double = sum1 / CDbl(n)
            Dim avg2 As Double = sum2 / CDbl(n)

            Console.WriteLine("-------------------------------------------------------------------")
            Console.WriteLine(" avg:  {0,18:F08}  {1,18:F08}  {2,18:F08}  {3,8:F04}", _
                avg1, avg2, avg2 - avg1, CalcRate(avg1, avg2))

            Console.WriteLine("-------------------------------------------------------------------")
            Console.WriteLine(" max:  {0,18:F08}  {1,18:F08}  {2,18:F08}  {3,8:F04}", _
                max1, max2, max2 - max1, CalcRate(max1, max2))

            Console.WriteLine("-------------------------------------------------------------------")
            Console.WriteLine(" min:  {0,18:F08}  {1,18:F08}  {2,18:F08}  {3,8:F04}", _
                min1, min2, min2 - min1, CalcRate(min1, min2))

            If zeros1 + zeros2 > 0 Then
                Console.WriteLine("-------------------------------------------------------------------")
                Console.WriteLine("zeros: {0,18:D}  {1,18:D}  {2,18:D}", _
                    zeros1, zeros2, zeros2 - zeros1)
            End If

            If minus1 + minus2 > 0 Then
                Console.WriteLine("-------------------------------------------------------------------")
                Console.WriteLine("minus: {0,18:D}  {1,18:D}  {2,18:D}", _
                    minus1, minus2, minus2 - minus1)
            End If

            Console.WriteLine("-------------------------------------------------------------------")
            Console.WriteLine(" rel:  {0,18:F08}  {1,18:F08}  {2,18:F08}  {3,8:F04}", _
                rel1 / CDbl(n), rel2 / CDbl(n), _
                (rel2 - rel1) / CDbl(n), CalcRate(rel1, rel2))

            Console.WriteLine("-------------------------------------------------------------------")
            Console.WriteLine(" rlSQ: {0,18:F08}  {1,18:F08}  {2,18:F08}  {3,8:F04}", _
                relSQ1 / CDbl(n), relSQ2 / CDbl(n), _
                (relSQ2 - relSQ1) / CDbl(n), CalcRate(relSQ1, relSQ2))

            Dim beatSc1 As Double = CDbl(beat1) / CDbl(n * 10)
            Dim beatSc2 As Double = CDbl(beat2) / CDbl(n * 10)
            Console.WriteLine("-------------------------------------------------------------------")
            Console.WriteLine(" beat: {0,18:F08}  {1,18:F08}  {2,18:F08}  {3,8:F04}", _
                beatSc1, beatSc2, beatSc2 - beatSc1, CalcRate(beatSc1, beatSc2))

            Console.WriteLine("-------------------------------------------------------------------")
            Console.WriteLine("changes +{0}/{4}/-{1}  (+{2:F03}/{5:F03}/-{3:F03})", _
                plus, minus, CDbl(plus) / CDbl(n), CDbl(minus) / CDbl(n), _
                n - plus - minus, CDbl(n - plus - minus) / CDbl(n))

        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

End Module