Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports System

Public Module Research

    Public Sub Main()
        Try
            Dim N As Integer = CInt(Console.ReadLine())
            Dim data(N - 1) As Integer
            For i As Integer = 0 To UBound(data)
                data(i) = CInt(Console.ReadLine())
            Next i

            Dim seed As Integer = CInt(Environment.GetCommandLineArgs()(1))

            Console.Error.WriteLine( _
                "seed: {0:D8}, Data size: {1,3:D}" _
                , seed, data.Count)

            Dim ret(-1) As Integer
            For i As Integer = 0 To UBound(ret)
                ret(i) = i
            Next i

            Console.WriteLine(ret.Length)
            For Each r As Integer In ret
                Console.WriteLine(r)
            Next r
            Console.Out.Flush()

        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

End Module