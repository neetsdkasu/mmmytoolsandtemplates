@ECHO OFF
IF NOT "%~2"=="" GOTO label2args
IF NOT "%~1"=="" GOTO label1arg
FOR /L %%I IN (1,1,500) DO @r.bat %%I
GOTO finally
:label1arg
FOR /L %%I IN (1,1,%1) DO @r.bat %%I
GOTO finally
:label2args
FOR /L %%I IN (%1,1,%2) DO @r.bat %%I %3 %4 %5 %6 %7 %8 %9
GOTO finally
:finally
