@echo off
setlocal
if "%~1"=="" goto errlabel
if "%~1"=="local" goto movetolocal

set OUTDIR=.\public\submit%~1

goto domove

:movetolocal

if "%~2"=="" goto errlabel

set OUTDIR=.\local\submit%~2

goto domove


:domove

if not exist "%OUTDIR%" goto extlabel

if exist "%OUTDIR%\1.png" goto extplabel

:doit

echo move to "%OUTDIR%\"

move *.png "%OUTDIR%\"

echo done.

goto endlabel


:errlabel
echo need arg .... submit num !
goto endlabel

:extlabel
echo not exist directory "%OUTDIR%"
goto endlabel


:extplabel
if "%~3"=="-f" goto doit
if "%~3"=="/f" goto doit
if "%~3"=="force" goto doit
echo already exist "%OUTDIR%\1.png"
goto endlabel

:endlabel
endlocal
