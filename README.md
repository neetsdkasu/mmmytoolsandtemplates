MMMyToolsAndTemplates
=====================

TCMMで使うツールと雛形ファイル  
ちらほらバグなどがあって適宜修正しながら使ってる  


`./files/`下に当該ファイル群がある  


※IDEは使わず、Notepad++などのテキストエディタとコマンドプロンプトとGit for Windows(Git Bash)でMMしてる  




## 説明

##### README.md (雛形)
 
MMのタイトルと問題文のリンクと順位表のリンクを記載するための雛形  


##### LICENSE (雛形)

MMのコードのライセンス  
※公開リポジトリはオープンソースである必要があるんじゃなかったっけ？(よく知らん)  


##### .gitignore (雛形)

たいていリポジトリに保存しないであろうファイルの拡張子が含まれているだけ  
`*.exe`や`*.pdb`や`*.jar`など　　


##### vbc.bat (ツール)

VisualBasic.NETのコンパイラを呼び出すだけのバッチファイル   
`PATH`に一時的に.NET Frameworkのディレクトリ登録する手間を省くため  
このバッチファイルへの引数はすべて`vbc.exe`に渡される  
`vbc.exe`の絶対パスで実行しているため、PCによって変えたほうがいいかも  
```
vbc.bat /nologo Solution.vb Main.vb
```


##### compile.bat (ツール)

MMのコードをデバッグモードでコンパイルするバッチファイル  
引数は受け取らないようになってる  
コンパイル成功で`Main.exe`と`Main.pdb`が作成される  
デバッグビルドなため実行は遅い(PCスペック依存)   
```
compile.bat
```


##### Solution.vb (雛形)

コンテスト問題が要求するクラスを実装するためのコードの雛形  
Javaと違いファイル名とクラス名を一致させる必要はないためSolution.vbのまま使える    
`compile.bat`内のファイル名指定を変更する必要がなくなる利点がある   


##### Main.vb (雛形)

オフラインテスターとやりとりをする部分を実装するためのコードの雛形   


##### a.bat (ツール)

オフラインテスターでテストを実行するバッチファイル 
実行ファイルは指定済みなので`a.bat`叩くだけで実行できる   
引数なしだとseed=1が実行される  
第1引数にseed番号を第2引数以降はそのままオフラインテスター自体の引数として渡される  
例えばseed=83で`-novis`指定なら`a.bat 83 -novis`とすればよい   
```
a.bat

a.bat 58

a.bat 83 -novis
```


##### f.bat (ツール)

複数seedで`a.bat`を実行するバッチファイル  
引数なしだとseed=1～seed=10までを実行する  
引数1つだと終了seed指定となりseed=1から指定seedまで実行される、例えばseed=83までなら`f.bat 83`  
引数2つだと開始seedと終了seed指定となる。また引数3つ目以降はオフラインテスターに渡す引数となる  
例えば`f.bat 1983 2018 -novis`はseed=1983～seed=2018までのテストで全てのテストで`-novis`をテスターに渡す  
```
f.bat

f.bat 83

f.bat 1001 2000

f.bat 1983 2018 -novis

f.bat 1 100 > score.txt
```


##### optimize.bat (ツール)

MMのコードを最適化モードでコンパイルするバッチファイル  
引数は受け取らないようになってる  
コンパイル成功で`Main.exe`が作成される  
デバッグビルドでは実行時間に困るときに使う  
エラー時が怖い   
```
optimize.bat
```


##### Counter.exe (ツール)

この`MMMyToolsAndTemplates`リポジトリには実行ファイル形式としては入っておらず、
ビルドするためのソースファイル`Counter.vb`とコンパイルバッチ`compileCounter.bat`が含まれている  
バッチを実行すれば`Counter.exe`がビルドされる  

合計スコアを計算するだけ。パイプで`f.bat`と組み合わせたりして使う  

引数を指定なしだと合計スコアが表示されるだけとなる  
引数に何か置くと入力の`Score = ～`の行も列挙される  
```
f.bat 1 10 | Counter.exe

f.bat | Counter.exe show

Counter.exe < score.txt
```

##### DiffScore.exe (ツール)

この`MMMyToolsAndTemplates`リポジトリには実行ファイル形式としては入っておらず、
ビルドするためのソースファイル`DiffScore.vb`とコンパイルバッチ`compileDiffScore.bat`が含まれている  
バッチを実行すれば`DiffScore.exe`がビルドされる  

テストケースごとのスコア比較(増減)、および総合スコアの比較を行う  

ファイルに保存した`f.bat`での出力同士(スコア同士)を比較するのに使われる  
スコアファイルには`Score = ～`の形式でスコアが書かれている必要がある  
スコア比較は`Score = ～`の登場順に行わせるため比較ファイルは同一のテストケース群の結果である必要がある  

引数にファイルを２つ指定するとその２つのファイルのスコアを比較する  
引数にファイルを１つ指定するとそのファイルと標準入力からのスコアとを比較する(なお標準入力は`_stdin.txt`に保存される)  

※バグがありファイルに含まれるテストケース数に違いがあると総合スコア等が正しく表示されない(いつか修正する予定)  
```
DiffScore.exe score1.txt score2.txt

f.bat 1 100 | DiffScore.exe score1.txt
```


##### doResearch.bat ( r.bat ) (ツール)

`doResearch.bat`は`r.bat`を呼び出し、`r.bat`は`Research.exe`を必要とする  

この`MMMyToolsAndTemplates`リポジトリには`Research.exe`は入っておらず、  
ビルドするためのソースファイル`Research.vb`とコンパイルバッチ`compileResearch.bat`が含まれている   
バッチを実行すれば`Research.exe`がビルドされる  


ファイル的には  
`Research.vb`は`Main.vb`相当であり  
`r.bat`は`a.bat`相当であり  
`doResearch.bat`は`f.bat`相当である    

テストケースを分析するためのコードを`Research.vb`に書いてテスターに実行ファイルとして渡す  
`Research.vb`内で標準エラー出力として分析データを出力しテスターがそれを標準出力に吐き出すことなる  
`Research.exe`には`seed`値が引数として渡されるため分析結果をファイル保存するように書くことも可能  
`Research.vb`はMMごとに毎回書き直す   
理論値スコアの算出などにも使ったりする  


引数は`r.bat`は`a.bat`と同様であり、`doResearch.bat`は`f.bat`と同様である  
```
r.bat 83 -novis

doResearch.bat 1 100 -novis > data.txt
```


##### public/index.html public/myscript.js (ツール)

適当な区切り(例えばサブミット)ごとのスコアを比較したものを表示するためのツール  

対象スコアファイルは`public/submit3/score.txt`というような感じに区切りごとに連番で保存されていることを前提とする  
`score.txt`は`f.bat`による出力を想定して処理している  
理論値スコアなどが存在する場合は`public/data.txt`に書いておく、形式は`myscript.js`内の`TSCORE_PARSER`の正規表現に従う    
またビジュアライザによって画像ファイルが保存される場合も表示することができる、その場合は`public/submit3/7.png`などテストケース番号のファイル名で保存されていることを前提とする  


画像を表示する場合は`index.html`の冒頭の`CSS`の設定で画像サイズなどの指定する必要があり手間  

`myscript.js`の冒頭にスコアの比較方法などを設定する変数が書いてあり、MMごとに書き換えて使う  

```javascript
var USE_IMAGE = false;  // 画像を表示するかどうか。表示true、非表示false
var SUBMITS = 100; // 区切り(サブミットなど)の最大数
var SCORE_TYPE = [ABS, REL, BEAT][1]; // スコア比較のタイプの選択(indexで) ABS(絶対値) REL(相対値) BEAT(順位点)
var SCORE_ASC_ORDER = true; // スコアの順序、小さいスコアが良いスコアならtrue、大きいスコアが良いスコアならfalse
var FIRST_SEED_IN_TXT  = 1; // score.txtの最初のケースのseed値
var LAST_SEED_IN_TXT   = 50; // score.txtの最後のケースのseed値
var FIRST_SEED_OF_VIEW = FIRST_SEED_IN_TXT; // 表示に使われる最初のseed (現在機能しておらず)
var LAST_SEED_OF_VIEW  = LAST_SEED_IN_TXT; // 表示に使われる最後のseed (現在機能しておらず)
var DEFAULT_VIEW_SEED  = 1; // 画像表示する場合、ページを開いたときに表示される画像のseed
var SCORE_LEN = 5;  // スコア表示の小数点以下の桁数。最低2桁は表示される
var USE_TSCORE = false; // 理論値スコアを使うかどうか
var TSCORE_PARSER = /MaxScore:\s+(\d+)/; // 理論値スコアのファイルのスコア表示の形式  
```


##### doPublish.bat (ツール)

`f.bat`を実行しその結果を`score.txt`に記録して`public/submit??/`に移動させるバッチ  
このバッチ自体は完成しておらず現状は`public/`ディレクトリではなく`local/`ディレクトリにしかファイルを移動できない  
作り直そうとした`doPublish_x.bat`も存在する  

現在実行可能な引数の取り方は
```
: localディレクトリに新規にseed=1～100の結果を生成する
doPublish.bat local 100

: local ディレクトリのNo.3のデータをseed=1～100で更新する
doPublish.bat 3 local 100

: localディレクトリに新規に-novisフラグ付きでseed=1～10の結果を生成する
doPublish_x.bat local 1 10 -novis

: local ディレクトリのNo.3のデータを-novisフラグ付きでseed=1～10で更新する
doPublish_x.bat 3 local 1 10 -novis
```
`local`の指定を無くすと`public`にデータを生成する予定だったがまだ実装されていない...   


##### .gitlab-ci.yml (雛形)

`public/`にあるスコア結果をgitlab上で公開する場合に使用するファイル  
`pages`ブランチで`public/`下にあるファイル全てを公開できる  
通常は最終サブミット後に`pages`ブランチを作り`public/`のファイルをコミットする  









